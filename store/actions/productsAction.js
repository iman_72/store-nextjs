import axios from "axios";
import React , {useEffect , useState} from "react";

const getProducts = () =>{
    const [products , setProducts] = useState();

    useEffect(() =>{
        fetch('https://dummyjson.com/products')
        .then(response => response.json())
        .then((data) => {
            const transformedProduct = data.products.map(productData => {
            return{
                id: productData.id,
                title: productData.title,
                price: productData.price,
                description: productData.description,
                category: productData.category,
                image: productData.thumbnail,
            }
            });
            setProducts(transformedProduct);
        });

        // axios.get('https://dummyjson.com/productsds')
        // .then(res => {
        //     const transformedProduct = res.data.products.map(productData => {
        //             return{
        //                 id: productData.id,
        //                 title: productData.title,
        //                 price: productData.price,
        //                 description: productData.description,
        //                 category: productData.category,
        //                 image: productData.thumbnail,
        //             }
        //             });
        //             setProducts(transformedProduct);
        // }).catch(err => {
        //     console.log(err)
        // })
    } , [])
    return products;
}

export default getProducts;