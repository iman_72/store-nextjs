import { createStore , combineReducers , applyMiddleware } from "redux";


import { createSlice , configureStore } from '@reduxjs/toolkit';
import productSlice from "./reducer/ProductReducer";
import cartSlice from "./reducer/cardReducer";

// const store = createStore(
//     productReducer,
//     applyMiddleware(thunk),
// );
// console.log(store.getState())

// const store = createStore(productSlice);

const store = configureStore({
    reducer: {product: productSlice.reducer , cart: cartSlice.reducer},
})
export default store;