import React , {useState , useEffect} from "react";
import { createSlice } from "@reduxjs/toolkit";
import Cookies from "js-cookie";
import { getCookies, getCookie, setCookies, removeCookies } from 'cookies-next';


const initialcardState = {cart: [] , totoalQuantity: 0 , totoalAmount: 0}
const cartSlice = createSlice({
    name: 'product',
    initialState: initialcardState,
    reducers: {
        addToCart(state , action){
            const newItem = action.payload;
            const existingItem = state.cart.find(item => item.id === newItem.id);
            if(!existingItem){
                state.cart.push({
                    id: newItem.id,
                    image: newItem.image,
                    title: newItem.title,
                    description: newItem.description,
                    price: newItem.price,
                    quantity: 1,
                });
                state.totoalQuantity++;
                state.totoalAmount = state.totoalAmount + newItem.price;
               
                // Cookies.set('quantitiy', state.totoalQuantity)
                // localStorage.setItem('cartData' , JSON.stringify(state.cart.map(item => item)));
                // localStorage.setItem('totalQuantity' , JSON.stringify(state.totoalQuantity));
                // localStorage.setItem('totalAmount' , JSON.stringify(state.totoalAmount));
                // console.log(localStorageData);
            }else{
                existingItem.quantity++;
                state.totoalQuantity++;
                state.totoalAmount =state.totoalAmount + newItem.price;
                // Cookies.set('quantitiy', state.totoalQuantity)
                // localStorage.setItem('cartData' , JSON.stringify({
                //     cart: state.cart,
                //     quantitiy: state.totoalQuantity,
                //     amount: state.totoalAmount
                // }));
            }  
        },
        removeFromCart(state , action){
            const id = action.payload;
            const existingItem = state.cart.find(item => item.id === id);
            if(existingItem?.quantity === 1){
                state.cart = state.cart.filter(item => item.id !== existingItem.id);
                state.totoalAmount = state.totoalAmount - existingItem.price;
                state.totoalQuantity--;
            }else{
                existingItem.quantity--;
                state.totoalQuantity--;
                state.totoalAmount = state.totoalAmount - existingItem.price;
            }
        }
    }
})
export const cartActions = cartSlice.actions;
export default cartSlice;











