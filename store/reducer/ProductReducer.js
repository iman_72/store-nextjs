import React from "react";
import { createSlice } from "@reduxjs/toolkit";

const initialProductsState = {items:[]};
const productSlice = createSlice({
    name: 'product',
    initialState: initialProductsState,
    reducers: {
        addProducts(state , action){
            state.items = action.payload;
        }
    }
})
export const productsAction = productSlice.actions;
export default productSlice;