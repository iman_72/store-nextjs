import React , {useState} from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import AddMoreProduct from "../../components/Button/addMoreProduct";
import RemoveProductCart from "../../components/Button/removeProductCart";
import styles from "./Cart.module.css";

const Cart = props => {
    const cartData = useSelector(state => state.cart.cart);
    const totalCartAmaunt = useSelector(state => state.cart.totoalAmount);
    const totoalCartQuantity = useSelector(state => state.cart.totoalQuantity);

    return(
        <React.Fragment>
            <div>
                {cartData?.map(item => (
                    <div>
                        <div className={styles.cartWrapper}>
                            <div><img className={styles.cartImage} src={item.image}></img></div>
                            <div className={styles.cartItemInfo}>
                                <h3>{item.title}</h3>
                                <div>{item.description}</div>
                                <br></br>
                                <div>Item Price:  {item.price}</div>
                                <div>Quantity:  {item.quantity}</div>
                                <AddMoreProduct 
                                    key={item.id}
                                    id={item.id}
                                    title={item.title}
                                    price={item.price}
                                    description={item.description}
                                    image={item.image}
                                />
                                <RemoveProductCart id={item.id} />
                            </div>
                        </div>
                        <hr></hr>
                    </div>
                ))}
                {console.log(cartData)}
            </div>
            <div className={styles.cartSubTotal}>
                <h3>Totoal Price:   {totalCartAmaunt}</h3>
                <h3>Item Added:   {totoalCartQuantity}</h3>
            </div>
        </React.Fragment>
    )
}

export default Cart;