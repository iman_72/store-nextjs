import Cookies from "js-cookie";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import Link from 'next/link';
import React , {useEffect} from "react";
import styles from "./CartButton.module.css";

const CartButton = quantity => {
    const totoalCartQuantity = useSelector(state => state.cart.totoalQuantity);

    
    // let totoalCartQuantity;
    // if (typeof window !== 'undefined'){
    //      totoalCartQuantity = localStorage.getItem('totalQuantity');
    // }
    

    // let totoalCartQuantity = Cookies.get('cart');;

    // let totoalCartQuantity;
    // if (typeof window !== 'undefined') {
    //     totoalCartQuantity = 0;
    // }else{
    //     totoalCartQuantity = Cookies.get('cart')
    // }
    // console.log(quantity)
    return(
        <div className={styles.cartWrapper}>
            <Link className={styles.cart} href="Cart/Cart">
                <div className="cart-svg">
                    <svg fill="#1a879f" xmlns="http://www.w3.org/2000/svg" width="24" height="24"><path d="M16.159 22H7.868c-1.88 0-3.175-.438-3.958-1.339-.819-.941-1.082-2.361-.804-4.346l.777-6.195a4.083 4.083 0 0 1 1.193-2.413A2.736 2.736 0 0 1 6.932 7h10.155a2.667 2.667 0 0 1 1.827.731 4.391 4.391 0 0 1 1.23 2.389l.769 6.195a5.132 5.132 0 0 1-.906 4.23A4.914 4.914 0 0 1 16.159 22zm-1.273-11.671a.91.91 0 1 0 .884.91.9.9 0 0 0-.884-.91zm-5.789 0a.91.91 0 1 0 .884.91.9.9 0 0 0-.882-.91z"></path><path d="M16.974 6.774A.5.5 0 0 1 16.93 7h-1.437a.649.649 0 0 1-.044-.226 3.484 3.484 0 0 0-6.968 0 .649.649 0 0 1 0 .226H7.01a.649.649 0 0 1 0-.226 5 5 0 0 1 9.99 0z" opacity=".4"></path></svg>
                </div>
                <div className={styles.cartBadge}>
                    {totoalCartQuantity}
                </div>
            </Link>
        </div>
    )
}


export async function getServerSideProps(context) {
    const totoalCartQuantity = localStorage.getItem('totalQuantity');
    return {   
      props: [hi=>'hello'], // will be passed to the page component as props
    }
  }

export default CartButton;