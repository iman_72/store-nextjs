import React , {useState} from "react";
import { useDispatch } from "react-redux";
import { productsAction } from '../../store/index';
import { counterAction } from "../../store/index";
import { cartActions } from "../../store/reducer/cardReducer";
import styles from "./AddToCart.module.css";

const AddToCart = props => {
    const dispatch = useDispatch();

    const addToCartHandler = event => {
        // console.log(props.product.id)
        dispatch(cartActions.addToCart(props.product));
    }
    return(
        <div>
            <button className={styles.addToCart} onClick={addToCartHandler}>Add To Cart</button>
        </div>
    )
}

export default AddToCart;