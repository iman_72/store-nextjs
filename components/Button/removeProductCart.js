import { useDispatch } from "react-redux";
import { cartActions } from "../../store/reducer/cardReducer";
import styles from "./removeProductCart.module.css";


const addMoreProduct = props => {
    const dispatch = useDispatch();
    const addItemHandler = event => {
        dispatch(cartActions.removeFromCart(props.id));
    }
    return(
        <button className={styles.removeButton} onClick={addItemHandler}>-</button>
    )
}

export default addMoreProduct;