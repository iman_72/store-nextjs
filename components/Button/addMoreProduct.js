
import { useDispatch } from "react-redux";
import { cartActions } from "../../store/reducer/cardReducer";
import styles from "./addMoreProduct.module.css";


const addMoreProduct = props => {
    const dispatch = useDispatch();
    const addItemHandler = event => {
        dispatch(cartActions.addToCart(props));
    }
    return(
        <button className={styles.addButton} onClick={addItemHandler}>+</button>
    )
}

export default addMoreProduct;