import React , {useEffect , useState} from "react";
import { useSelector , useDispatch } from "react-redux";
import { productsAction } from "../../store/reducer/ProductReducer";

const API = () =>{
    const [products , setProducts] = useState();
    const dispatch = useDispatch();

    useEffect(() =>{
        fetch('https://dummyjson.com/products')
        .then(response => response.json())
        .then((data) => {
            const transformedProduct = data.products.map(productData => {
            return{
                // id: productData.episode_id,
                // title: productData.title,
                // openingText: productData.opening_crawl,
                id: productData.id,
                title: productData.title,
                price: productData.price,
                description: productData.description,
                category: productData.category,
                image: productData.thumbnail,
            }
            });
            setProducts(transformedProduct);
        });
    } , [])
        // console.log(products)
        // dispatch({type: 'addProducts' , payload: products})
        dispatch(productsAction.addProducts(products))

}

export default API;