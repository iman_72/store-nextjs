import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import Product from './Product';
import { productsAction } from '../../store/index';
import store from '../../store/index';
import styles from "./ProductsList.module.css";


const ProductList = props => {
    const productsData = useSelector(state => state.product.items);
    return(
        <section>
            <div className={styles.productsWrapper}>
            {productsData?.map((item) =>(
                <Product
                    key={item.id}
                    id={item.id}
                    title={item.title}
                    price={item.price}
                    description={item.description}
                    category={item.category}
                    // image={item.image}
                />
            ))}
            
            </div>
        </section>
    )
}

export default ProductList;

