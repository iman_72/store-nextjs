import React from 'react';
import AddToCart from '../Button/AddToCart';
import { useSelector } from 'react-redux';
import styles from "./Product.module.css";
const Product = props => {
    const data = useSelector(state => state.item);
    const selectedProduct = {
      id: props.id,
      title: props.title,
      description: props.description,
      price: props.price,
      image: props.image
    }
    // console.log(props)
    return(
    <div className={styles.productsCard}>
      <div>
        <div> 
          <img  className={styles.productsImage} src={props.image}></img>
        </div>
        <div className={styles.productsDitalis}>
          <h3 className={styles.producstTitle}>{props.title}</h3>
          <p className={styles.producstDes}>{props.description}</p>
          <h4 className={styles.productsPrice}>{props.price} $</h4>
        </div>
        <div>
          <AddToCart
            product={selectedProduct}
          />
        </div>
      </div>
    </div>
    )
}

export default Product;